<?php

namespace App\Doctrine;


use Doctrine\Bundle\DoctrineBundle\ConnectionFactory as ConnectionFactoryBase;
use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Types\Type;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

class ConnectionFactory
{
    private $serializer;

    private $connectionFactory;

    private $initialized = false;

    private $typesConfig;

    public function __construct(array $typesConfig, SerializerInterface $serializer, ConnectionFactoryBase $connectionFactory)
    {
        $this->serializer = $serializer;
        $this->connectionFactory = $connectionFactory;
        $this->typesConfig = $typesConfig;
    }

    /**
     * @param array $params
     * @param Configuration|null $config
     * @param EventManager|null $eventManager
     * @param array $mappingTypes
     * @return \Doctrine\DBAL\Connection
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createConnection(array $params, Configuration $config = null, EventManager $eventManager = null, array $mappingTypes = [])
    {
        $connection = $this->connectionFactory->createConnection($params, $config, $eventManager, $mappingTypes);
        if (!$this->initialized) {
            foreach ($this->typesConfig as $type => $typeConfig){
                if (Type::hasType($type)) {
                    $object = Type::getType($type);
                    if($object instanceof  SerializerAwareInterface){
                        $object->setSerializer($this->serializer);
                    }
                }
            }
            $this->initialized = true;
        }

        return $connection;
    }


}
