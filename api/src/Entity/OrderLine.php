<?php

namespace App\Entity;


use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;


class OrderLine
{
    /**
     * @Groups({"order:read"})
     */
    private $name;

    /**
     * @Groups({"order:read"})
     */
    private $price;

    /**
     * @Groups({"order:read"})
     */
    private $quantity;

    public function __construct(string $name, string $price, int $quantity)
    {
        if($price < 0 ){
            throw new \Exception("Price should be greater than 0");
        }

        if($quantity < 0){
            throw new \Exception("Quantity should be greater than 0");
        }

        $this->name = $name;
        $this->price = $price;
        $this->quantity = $quantity;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }
}
