<?php

namespace App\CQRS\Handler;


use App\CQRS\BaseResult;
use App\CQRS\Command\Order as OrderCommand;
use App\Entity\Order;
use App\Entity\OrderLine;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class OrderHandler implements MessageHandlerInterface
{
    private $productRepository;

    private $entityManager;

    public function __construct(ProductRepository $productRepository, EntityManagerInterface $entityManager)
    {
        $this->productRepository = $productRepository;
        $this->entityManager = $entityManager;
    }

    public function __invoke(OrderCommand $order)
    {
        $orderLines = [];
        foreach ($order->getOrderLines() as $orderLine) {
            $product = $this->productRepository->find($orderLine->getProduct());
            $orderLines[] = new OrderLine(
                $product->name,
                $product->price,
                $orderLine->getQuantity()
            );
        }

        $this->entityManager->persist(new Order($order->getId(), $order->getTotal(), $orderLines));
        $this->entityManager->flush();

        return new BaseResult($order->getId());
    }
}
