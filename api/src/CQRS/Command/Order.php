<?php

namespace App\CQRS\Command;

use ApiPlatform\Core\Annotation\ApiResource;
use App\CQRS\BaseResult;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"order:add"}},
 *     normalizationContext={"groups"={"order:add", "base-result:read"}},
 *     messenger=true,
 *     collectionOperations={
 *         "post"={ "status" = 202 }
 *     },
 *     itemOperations={},
 *     outputClass=BaseResult::class
 * )
 */
class Order
{
    /**
     * @Groups("order:add")
     */
    private $id;

    /**
     * @Groups("order:add")
     */
    private $total;

    /**
     * @Groups("order:add")
     * @var OrderLine[]
     */
    private $orderLines;

    public function __construct( $total = null, array $orderLines = [])
    {
        $this->id = (string)Uuid::uuid4();
        $this->total =(int) $total;
        $this->orderLines = $orderLines;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function getOrderLines(): array
    {
        return $this->orderLines;
    }
}
