<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *   attributes={
 *     "access_control"="is_granted('IS_AUTHENTICATED_FULLY')"
 *   },
 *   collectionOperations={
 *     "get"={
 *        "access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"
 *     },
 *     "post"
 *   },
 *   itemOperations={
 *     "get"={
 *        "access_control"="is_granted('IS_AUTHENTICATED_ANONYMOUSLY')"
 *     },
 *     "put",
 *     "delete"
 *   }
 * )
 *
 * @ApiFilter(
 *     SearchFilter::class,
 *     properties={
 *          "id": "exact",
 *          "name": "partial"
 *     }
 * )
 */
class Category
{
    /**
     * @Groups("category:read")
     */
    private $id;
    /**
     * @Groups({"category:read", "category:add"})
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    public $name;

    /**
     * @ApiSubresource()
     */
    public $products;

    public function __construct()
    {
        $this->id = (string)Uuid::uuid4();
    }

    public function getId()
    {
        return $this->id;
    }
}


