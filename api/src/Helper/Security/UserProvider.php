<?php

namespace App\Helper\Security;


use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

class UserProvider implements UserProviderInterface
{
    /**
     * @var User[]
     */
    private $users = [];

    public function createUser(string $id, string $token, string $role = 'ROLE_USER')
    {
        $this->users[$id] = new User($id, $token, $role);
    }

    public function loadUserByUsername($username)
    {
        foreach ($this->users as $user) {
            if($user->getUsername() === $username){
                return $user;
            }
        }
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $storedUser = $this->loadUserByUsername($user->getUsername());

        return new User($storedUser->getId(), $storedUser->getUsername(), $storedUser->getRoles());

    }

    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
