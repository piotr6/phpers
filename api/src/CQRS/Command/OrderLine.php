<?php

namespace App\CQRS\Command;



use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     denormalizationContext={"groups"={"order:add"}},
 *     normalizationContext={"groups"={"order:add"}},
 *     collectionOperations={},
 *     itemOperations={},
 * )
 */
class OrderLine
{
    /**
     * @Groups("order:add")
     */
    private $product;
    /**
     * @Groups("order:add")
     */
    private $quantity;

    public function __construct($product = null, $quantity = null)
    {
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProduct(): ?string
    {
        return $this->product;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }
}
