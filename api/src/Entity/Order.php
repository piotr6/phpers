<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"order:read"}},
 *     collectionOperations={
 *         "get"
 *     },
 *     itemOperations={
 *         "get"
 *     }
 * )
 */
class Order
{
    /**
     * @Groups({"order:read"})
     */
    private $id;

    /**
     * @Groups({"order:read"})
     */
    private $total;

    /**
     * @Groups({"order:read"})
     * @var OrderLine[]
     */
    private $orderLines;

    /**
     * Order constructor.
     * @param string $id
     * @param int $total
     * @param OrderLine[] $orderLines
     * @throws \Exception
     */
    public function __construct(string $id, int $total, array $orderLines)
    {
        $this->id = $id;
        $this->orderLines = [];
        $orderLinesTotal = 0;
        foreach ($orderLines as $orderLine) {
            if (!$orderLine instanceof OrderLine) {
                throw new \Exception('Wrong type');
            }

            $this->orderLines[] = $orderLine;
            $orderLinesTotal += $orderLine->getQuantity() * $orderLine->getPrice();
        }

        if ($orderLinesTotal !== $total) {
            throw new \Exception('Wrong total');
        }

        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getTotal(): int
    {
        return $this->total;
    }

    /**
     * @return OrderLine[]
     */
    public function getOrderLines(): array
    {
        return $this->orderLines;
    }
}
