<?php

namespace App\CQRS;


use Symfony\Component\Serializer\Annotation\Groups;

class BaseResult
{
    /**
     * @Groups({"base-result:read"})
     */
    private $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
