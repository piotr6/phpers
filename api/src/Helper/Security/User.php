<?php

namespace App\Helper\Security;


use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface
{
    private $id;
    private $token;
    private $role;

    public function __construct(string $id, string $token, string $role)
    {
        $this->id = $id;
        $this->token = $token;
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    public function getRoles()
    {
        return [$this->role];
    }

    public function getPassword()
    {
        return '';
    }

    public function getSalt()
    {
        return '';
    }

    public function getUsername()
    {
       return $this->token;
    }

    public function eraseCredentials()
    {
    }
}
