<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"product:read"}},
 *     denormalizationContext={"groups"={"product:add"}},
 *     collectionOperations={
 *         "get",
 *         "post" = {
 *             "normalization_context"={"groups"={"product:read", "product:detail"}},
 *              "validation_groups"={"product:validation"}
 *         },
 *     },
 *     itemOperations={
 *         "get"={
 *             "normalization_context"={"groups"={"product:read", "product:detail"}}
 *         },
 *         "getDetail"={
 *              "method"="get",
 *              "path"="/products/{id}/detail"
 *         },
 *         "delete"
 *     }
 * )
 */
class Product
{
    /**
     * @Groups("product:read")
     */
    private $id;
    /**
     * @Assert\NotBlank(groups={"product:validation"})
     * @Assert\Length(max=50, groups={"product:validation"})
     * @Groups({"product:read", "product:add"})
     */
    public $name;
    /**
     * @Assert\NotBlank(groups={"product:validation"})
     * @Groups({"product:read", "product:add"})
     */
    public $price;

    /**
     * @Assert\NotBlank(groups={"product:validation"})
     * @Groups({"product:detail", "product:add"})
     */
    public $category;

    public function __construct()
    {
        $this->id = (string)Uuid::uuid4();
    }

    public function getId()
    {
        return $this->id;
    }
}
